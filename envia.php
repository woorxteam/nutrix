<?php
require '/PHPMailer/PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Set who the message is to be sent from
$mail->setFrom('alex@worx.mx', 'Alejandro García');
//Set an alternative reply-to address
$mail->addReplyTo('alex@woorx.mx', 'Alejandro García');
//Set who the message is to be sent to
$mail->addAddress('alex@woorx.mx', 'Alejandro García');
//Set the subject line
$mail->Subject = 'PHPMailer mail() test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
$mail->addAttachment('/PHPMailer/images/phpmailer_mini.png');



//send the message, check for errors
if (!$mail->send()) {
    echo "Ocurrio un error al enviar el mensaje, intente de nuevo.";
} else {
    echo "Gracias por su envío, nos pondremos en contacto en breve.";
}
