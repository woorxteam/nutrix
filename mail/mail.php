<?php
require '../PHPMailer/PHPMailerAutoload.php';

$contenido = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <title>Nutrix</title>
</head>
<body>
<div style='width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;'>
  <h1>Contacto desde el sitio web de Nutrix.</h1>
  <p><strong>Nombre: </strong>. ".utf8_decode($_POST['name'])."</p>
  <p><strong>Tel&eacute;fono: </strong>. ".utf8_decode($_POST['phone'])."</p>
  <p><strong>Email: </strong>. ".utf8_decode($_POST['email'])."</p>
  <p><strong>Mensaje: </strong>. ".nl2br(utf8_decode($_POST['message']))."</p>
</div>
</body>
</html>";

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Set who the message is to be sent from
$mail->setFrom('susana.hernandez@nutrix.mx', 'Nutrix');
//Set an alternative reply-to address
$mail->addReplyTo('susana.hernandez@nutrix.mx', 'Nutrix');
//Set who the message is to be sent to
$mail->addAddress('susana.hernandez@nutrix.mx', 'Nutrix');
//Set the subject line
$mail->Subject = 'Contacto desde el sitio web de Nutrix';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($contenido, dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'Contacto desde el sitio web de Nutrix';
//Attach an image file
//$mail->addAttachment('../PHPMailer/images/phpmailer_mini.png');



//send the message, check for errors
if (!$mail->send()) {
    echo "Ocurrio un error al enviar el mensaje, intente de nuevo.";
} else {
    echo "Gracias por su envío, nos pondremos en contacto en breve.";
}
